
public class MonitoredData {
    private String startDate;
    private String endDate;
    private String startTime;
    private String endTime;
    private String activityLabel;

    public MonitoredData(String startDate,String startTime, String endDate, String endTime, String activityLabel) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.activityLabel = activityLabel;
    }

    public int valueint(){
        return activityLabel.length();
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getActivityLabel() {
        return activityLabel;
    }

    public void setActivityLabel(String activityLabel) {
        this.activityLabel = activityLabel;
    }

    @Override
    public String toString() {
        return "MonitoredData{" + "startDate='" + startDate + '\'' + ", endDate='" + endDate + '\'' + ", startTime='" + startTime + '\'' + ", endTime='"
            + endTime + '\'' + ", activityLabel='" + activityLabel + '\'' + '}';
    }
}
