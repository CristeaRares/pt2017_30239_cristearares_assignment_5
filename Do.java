import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Do {
	private static List<MonitoredData> activities = new ArrayList<>();
	private Scanner x;
	
	public void openFile(){
		try{
			x=new Scanner(new File("activities.txt"));
		}catch(Exception e){
			System.out.println("could not find file");
		}
	}
	
	

	public void readFile(){
		while(x.hasNext()){
			  String a=x.next();
			  String b=x.next();
			  String c=x.next();
			  String d=x.next();
			  String e=x.next();
			  
			  //System.out.printf("%s %s %s %s %s\n",a,b,c,d,e);
			  
			   activities.add(new MonitoredData(a,b,c,d,e));
			   
			  
		}
	}
	
	public void closeFile(){
		x.close();
	}
	public void distinctDays()  {
		  List<String> startDates = activities.stream().map(MonitoredData::getStartDate).distinct().collect(Collectors.toList());
	        List<String> endDates = activities.stream().map(MonitoredData::getEndDate).distinct().collect(Collectors.toList());

	        startDates.addAll(endDates);
	        System.out.println("Distinct Dates: " + startDates.stream().distinct().count());
	        System.out.println("--------------------------------------------------------------");
	       
	}
	
	public void distinctActions() throws IOException{
		 Map<String, Long> collect = activities.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.counting()));
	        for (String key : collect.keySet()) {
	            System.out.println(key + " --->" + collect.get(key));
	        }
	        System.out.println("--------------------------------------------------------------");
	        PrintWriter out = new PrintWriter(new File("distinctDays.txt"));


	        for (Map.Entry<String,Long> entry : collect.entrySet()) {
	            out.println(entry.getKey() + "\t=>\t" + entry.getValue());
	        }

	        out.close();
	}
	
	public void activityCount(){
		 Map<String, Collection<List<MonitoredData>>> collect1 = activities.stream().collect(Collectors
		            .groupingBy(MonitoredData::getStartDate, Collectors.collectingAndThen(Collectors.groupingBy(MonitoredData::getActivityLabel), Map::values)));
		       
		        System.out.println("--------------------------------------------------------------");
	}
	
	public void totalDuration() throws IOException{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        Map<String, Long> activityPerDuration = activities.stream()
            .collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.summingLong(value -> {
                return ChronoUnit.SECONDS.between(LocalDateTime.parse(value.getStartDate() + " " + value.getStartTime(), formatter),
                    LocalDateTime.parse(value.getEndDate() + " " + value.getEndTime(), formatter));
            })));
        for (String key : activityPerDuration.keySet()) {
            System.out.println(key + " --->" + activityPerDuration.get(key));
        }

        List<String> activitiesThatTakeMoreThenTenHours = activityPerDuration.entrySet().stream().filter(entry -> entry.getValue() > 10 * 60 * 60)
            .map(Map.Entry::getKey).collect(Collectors.toList());
        activitiesThatTakeMoreThenTenHours.forEach(System.out::println);
        
        
        PrintWriter out = new PrintWriter(new File("totalDuration.txt"));


        for (Map.Entry<String,Long> entry : activityPerDuration.entrySet()) {
            out.println(entry.getKey() + "\t=>\t" + entry.getValue());
        }
        out.println("\nActivities that take more then 10 hours:");
        for (int i=0;i<activitiesThatTakeMoreThenTenHours.size();i++) {
            out.println(activitiesThatTakeMoreThenTenHours.get(i) );
        }

        out.close();

        System.out.println("--------------------------------------------------------------");
	}
	
	public void filterActivities() throws IOException{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		 Map<String, List<MonitoredData>> collect3 = activities.stream().collect(Collectors.groupingBy(MonitoredData::getActivityLabel, Collectors.toList()));
	        List<String> collect4 = collect3.entrySet().stream().filter(entry -> {
	            List<MonitoredData> values = entry.getValue();
	            List<Long> samplesDurations = values.stream().map(value -> {
	                return ChronoUnit.SECONDS.between(LocalDateTime.parse(value.getStartDate() + " " + value.getStartTime(), formatter),
	                    LocalDateTime.parse(value.getEndDate() + " " + value.getEndTime(), formatter));
	            }).collect(Collectors.toList());

	            int total = samplesDurations.size();
	            long underFiveMinutes = samplesDurations.stream().filter(value -> value < 5 * 60).count();
	            return (90 * total) / 100 < underFiveMinutes;
	        }).map(Map.Entry::getKey).collect(Collectors.toList());
	        collect4.forEach(System.out::println);
	        PrintWriter out = new PrintWriter(new File("filterActivities.txt"));


	        for (int i=0;i<collect4.size();i++) {
	            out.println(collect4.get(i) );
	        }

	        out.close();
	        
	}
}
